import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState
import db.ServiceLocator

val db = ServiceLocator.contactRepo

@Composable
@Preview
fun App() {
    views.Main()
}

fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Compose Contacts",
        state = rememberWindowState(width = 400.dp, height = 600.dp),
        resizable = false
    ) {
        App()
    }
}
