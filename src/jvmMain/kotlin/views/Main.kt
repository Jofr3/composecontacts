package views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import db.ServiceLocator
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.runBlocking
import model.Contact
import java.util.*

val db = ServiceLocator.contactRepo

@Composable
@Preview
fun Main() {
    val contacts: List<Contact> = db.list()
    var searchContacts by remember { mutableStateOf(contacts.toMutableList()) }

    var screen by remember { mutableStateOf(0) }

    var currentContact by remember { mutableStateOf(Contact(UUID.randomUUID().toString(), "", "", "", "")) }

    var formName by remember { mutableStateOf("") }
    var formSurname by remember { mutableStateOf("") }
    var formNumber by remember { mutableStateOf("") }
    var formEmail by remember { mutableStateOf("") }

    var editName by remember { mutableStateOf("") }
    var editSurname by remember { mutableStateOf("") }
    var editNumber by remember { mutableStateOf("") }
    var editEmail by remember { mutableStateOf("") }

    var formError by remember { mutableStateOf(false) }

    Column(modifier = Modifier.background(Color.Black).fillMaxWidth()) {

        Row {
            Button(
                {
                    searchContacts = db.list().toMutableList()
                    screen = 0
                },
                Modifier
                    .offset { IntOffset(0, 0) }.padding(5.dp, 0.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
            ) {
                Text("Contacts", color = Color(5, 133, 255), fontSize = 16.sp, fontWeight = FontWeight.Bold)
            }

            Button(
                {
                    formName = ""
                    formSurname = ""
                    formNumber = ""
                    formEmail = ""
                    screen = 2
                }, Modifier
                    .offset { IntOffset(220, 0) }, colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
            ) {
                Text("+", color = Color(5, 133, 255), fontSize = 25.sp, fontWeight = FontWeight.Bold)
            }
        }

        Row(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {
            when (screen) {
                0 -> {
                    var search by remember { mutableStateOf("") }

                    Column {
                        OutlinedTextField(
                            value = search,
                            placeholder = {
                                Text(
                                    "Search",
                                    modifier = Modifier,
                                    color = Color(153, 153, 161),
                                    fontSize = 15.5.sp,
                                )
                            },
                            onValueChange = {
                                search = it
                                searchContacts = mutableListOf()
                                for (contact in contacts) {
                                    if ("${contact.name} ${contact.surname}".contains(search, ignoreCase = true)) {
                                        searchContacts.add(contact)
                                    }
                                }
                            },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier
                                .height(56.dp)
                                .padding(15.dp, 5.dp)
                                .fillMaxWidth()
                                .border(
                                    BorderStroke(
                                        width = 1.dp,
                                        color = Color.Transparent
                                    ),
                                ),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp,
                                color = Color.White,
                                lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            leadingIcon = {
                                Icon(
                                    Icons.Default.Search,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp),
                                )
                            }
                        )

                        Column(modifier = Modifier.padding(0.dp, 15.dp)) {
                            for (contact in searchContacts) {
                                Column(
                                    modifier = Modifier.padding(15.dp, 0.dp).verticalScroll(rememberScrollState(0))
                                        .clickable {
                                            runBlocking {
                                                currentContact = contact
                                                screen = 1
                                            }
                                        }) {
                                    Box(
                                        modifier = Modifier.fillMaxWidth().height(1.dp).background(Color(28, 28, 30))
                                    ) {}
                                    Text(
                                        "${contact.name} ${contact.surname}",
                                        color = Color.White,
                                        modifier = Modifier.padding(3.dp, 13.dp),
                                        fontSize = 17.sp
                                    )
                                }
                            }
                        }
                        VerticalScrollbar(
                            modifier = Modifier
                                .fillMaxHeight(),
                            adapter = rememberScrollbarAdapter(rememberScrollState(0))
                        )
                    }
                }

                1 -> {
                    Column(modifier = Modifier.padding(10.dp)) {
                        Text(
                            "${currentContact.name} ${currentContact.surname}",
                            modifier = Modifier.fillMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontSize = 30.sp,
                        )

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(62.dp)
                                .clip(shape = RoundedCornerShape(10.dp))
                                .background(Color(28, 28, 29))
                                .padding(10.dp, 10.dp)
                        ) {
                            Text(
                                "name",
                                color = Color.White,
                                fontSize = 16.sp,
                                modifier = Modifier.padding(0.dp, 2.dp)
                            )
                            Text(currentContact.name, color = Color(5, 133, 255), fontSize = 18.sp)
                        }

                        Column(modifier = Modifier.height(10.dp)) {}

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(62.dp)
                                .clip(shape = RoundedCornerShape(10.dp))
                                .background(Color(28, 28, 29))
                                .padding(10.dp, 10.dp)
                        ) {
                            Text(
                                "surname",
                                color = Color.White,
                                fontSize = 16.sp,
                                modifier = Modifier.padding(0.dp, 2.dp)
                            )
                            Text(currentContact.surname, color = Color(5, 133, 255), fontSize = 18.sp)
                        }

                        Column(modifier = Modifier.height(10.dp)) {}

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(62.dp)
                                .clip(shape = RoundedCornerShape(10.dp))
                                .background(Color(28, 28, 29))
                                .padding(10.dp, 10.dp)
                        ) {
                            Text(
                                "number",
                                color = Color.White,
                                fontSize = 16.sp,
                                modifier = Modifier.padding(0.dp, 2.dp)
                            )
                            Text(currentContact.number, color = Color(5, 133, 255), fontSize = 18.sp)
                        }

                        Column(modifier = Modifier.height(10.dp)) {}

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(62.dp)
                                .clip(shape = RoundedCornerShape(10.dp))
                                .background(Color(28, 28, 29))
                                .padding(10.dp, 10.dp)
                        ) {
                            Text(
                                "email",
                                color = Color.White,
                                fontSize = 16.sp,
                                modifier = Modifier.padding(0.dp, 2.dp)
                            )
                            Text(currentContact.email, color = Color(5, 133, 255), fontSize = 18.sp)
                        }

                        Column(modifier = Modifier.height(5.dp)) {}
                        Row {
                            Button(
                                {
                                    db.delete(currentContact.id)
                                    searchContacts = db.list().toMutableList()
                                    screen = 0
                                },
                                Modifier
                                    .offset { IntOffset(240, 0) },
                                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
                            ) {
                                Text(
                                    "Delete",
                                    color = Color.Red,
                                    fontSize = 16.sp,
                                    fontWeight = FontWeight.Bold
                                )
                            }

                            Button(
                                {
                                    editName = currentContact.name
                                    editSurname = currentContact.surname
                                    editNumber = currentContact.number
                                    editEmail = currentContact.email
                                    screen = 3
                                },
                                Modifier
                                    .offset { IntOffset(230, 0) },
                                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
                            ) {
                                Text("Edit", color = Color(5, 133, 255), fontSize = 16.sp, fontWeight = FontWeight.Bold)
                            }
                        }
                    }
                }

                2 -> {
                    Column(modifier = Modifier.padding(10.dp)) {
                        Text(
                            "New contact",
                            modifier = Modifier.fillMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontSize = 30.sp,
                        )

                        OutlinedTextField(value = formName, placeholder = {
                            Text("name", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { formName = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { formName = "" },
                                )
                            }
                        )

                        OutlinedTextField(value = formSurname, placeholder = {
                            Text("surname", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { formSurname = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { formSurname = "" },
                                )
                            }
                        )

                        OutlinedTextField(value = formNumber, placeholder = {
                            Text("number", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { formNumber = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { formNumber = "" },
                                )
                            }
                        )
                        OutlinedTextField(value = formEmail, placeholder = {
                            Text("email", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { formEmail = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { formEmail = "" },
                                )
                            }
                        )

                        Button(
                            {
                                if (formName == "" || formSurname == "" || formNumber == "" || formEmail == "") {
                                    formError = true
                                } else {
                                    formError = false
                                    db.insert(
                                        Contact(
                                            UUID.randomUUID().toString(),
                                            formName,
                                            formSurname,
                                            formNumber,
                                            formEmail
                                        )
                                    )
                                    searchContacts = db.list().toMutableList()
                                    screen = 0
                                }

                            },
                            Modifier
                                .offset { IntOffset(290, 0) },
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
                        ) {
                            Text("Done", color = Color(5, 133, 255), fontSize = 16.sp, fontWeight = FontWeight.Bold)
                        }

                        if (formError) {
                            Text(
                                "- One or more fields are empty!",
                                modifier = Modifier.fillMaxWidth().padding(20.dp),
                                textAlign = TextAlign.Start,
                                color = Color.Red,
                                fontSize = 15.sp,
                            )
                        }
                    }
                }

                3 -> {
                    Column(modifier = Modifier.padding(10.dp)) {
                        Text(
                            "Edit contact",
                            modifier = Modifier.fillMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontSize = 30.sp,
                        )

                        OutlinedTextField(value = editName, placeholder = {
                            Text("name", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { editName = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { editName = "" },
                                )
                            }
                        )

                        OutlinedTextField(value = editSurname, placeholder = {
                            Text("surname", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { editSurname = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { editSurname = "" },
                                )
                            }
                        )

                        OutlinedTextField(value = editNumber, placeholder = {
                            Text("number", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { editNumber = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { editNumber = "" },
                                )
                            }
                        )
                        OutlinedTextField(value = editEmail, placeholder = {
                            Text("email", modifier = Modifier, color = Color(153, 153, 161), fontSize = 15.5.sp)
                        },
                            onValueChange = { editEmail = it },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color(28, 28, 30),
                                focusedIndicatorColor = Color.Transparent
                            ),
                            modifier = Modifier.height(58.dp).padding(15.dp, 5.dp).fillMaxWidth()
                                .border(BorderStroke(width = 1.dp, color = Color.Transparent)),
                            shape = RoundedCornerShape(30),
                            textStyle = TextStyle(
                                fontSize = 15.5.sp, color = Color.White, lineHeight = 1.sp,
                            ),
                            singleLine = true,
                            trailingIcon = {
                                Icon(
                                    Icons.Default.Close,
                                    contentDescription = "",
                                    tint = Color(153, 153, 161),
                                    modifier = Modifier.width(55.dp)
                                        .clickable { editEmail = "" },
                                )
                            }
                        )

                        Button(
                            {
                                if (editName == "" || editSurname == "" || editNumber == "" || editEmail == "") {
                                    formError = true
                                } else {
                                    formError = false
                                    db.edit(Contact(currentContact.id, editName, editSurname, editNumber, editEmail))
                                    searchContacts = db.list().toMutableList()
                                    screen = 0
                                }
                            },
                            Modifier
                                .offset { IntOffset(290, 0) },
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Black)
                        ) {
                            Text("Done", color = Color(5, 133, 255), fontSize = 16.sp, fontWeight = FontWeight.Bold)
                        }

                        if (formError) {
                            Text(
                                "- One or more fields are empty!",
                                modifier = Modifier.fillMaxWidth().padding(20.dp),
                                textAlign = TextAlign.Start,
                                color = Color.Red,
                                fontSize = 15.sp,
                            )
                        }
                    }
                }
            }
        }
    }
}