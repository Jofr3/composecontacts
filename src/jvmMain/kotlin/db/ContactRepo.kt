package db

import model.Contact
import io.realm.kotlin.types.ObjectId

interface ContactRepo {
    fun list(): List<Contact>
    fun insert(contact: Contact)
    fun get(id: String): Contact
    fun delete(id: String)
    fun edit(contact: Contact)
}