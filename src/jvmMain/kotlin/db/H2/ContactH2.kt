package db.H2

import org.jetbrains.exposed.sql.Table

object ContactH2 : Table() {
    val id = varchar("id", 50)
    val name = varchar("name", 50)
    val surname = varchar("surname", 50)
    val number = varchar("number", 50)
    val email = varchar("email", 50)
}