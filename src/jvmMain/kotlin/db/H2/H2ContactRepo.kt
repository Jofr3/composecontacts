package db.Realm

import db.ContactRepo
import db.H2.ContactH2
import model.Contact
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction

class H2ContactRepo : ContactRepo {

    init {
        Database.connect("jdbc:h2:./myh2file", "org.h2.Driver")
        transaction {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(ContactH2)
            ContactH2.selectAll()
        }

    }

    override fun list(): List<Contact> {
        val contacts = mutableListOf<Contact>()
        transaction {
            ContactH2.selectAll().forEach {
                contacts.add(
                    Contact(
                        it[ContactH2.id],
                        it[ContactH2.name],
                        it[ContactH2.surname],
                        it[ContactH2.number],
                        it[ContactH2.email]
                    )
                )
            }
        }
        return contacts
    }

    override fun insert(contact: Contact) {
        transaction {
            ContactH2.insert {
                it[id] = contact.id
                it[name] = contact.name
                it[surname] = contact.surname
                it[number] = contact.number
                it[email] = contact.email
            }
        }
    }

    override fun get(id: String): Contact {
        val h2Object = transaction {
            ContactH2.select { ContactH2.id eq id }.first()
        }
        return Contact(
            h2Object[ContactH2.id],
            h2Object[ContactH2.name],
            h2Object[ContactH2.surname],
            h2Object[ContactH2.number],
            h2Object[ContactH2.email]
        )
    }

    override fun delete(id: String) {
        transaction {
            ContactH2.deleteWhere { ContactH2.id eq id }
        }
    }

    override fun edit(contact: Contact) {
        transaction {
            ContactH2.update({ ContactH2.id eq contact.id }) {
                it[id] = contact.id
                it[name] = contact.name
                it[surname] = contact.surname
                it[number] = contact.number
                it[email] = contact.email
            }
        }
    }
}
