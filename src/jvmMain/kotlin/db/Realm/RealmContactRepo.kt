package db.Realm

import db.ContactRepo
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import model.Contact

class RealmContactRepo : ContactRepo {
    private val realm: Realm

    init {
        val config = RealmConfiguration.Builder(setOf(ContactRealm::class))
            .deleteRealmIfMigrationNeeded()
            .build()
        realm = Realm.open(config)
    }

    override fun list(): List<Contact> {
        val contacts = mutableListOf<Contact>()
        val realmObjects = realm.query<ContactRealm>().find()
        realmObjects.forEach { realmObject ->
            contacts.add(
                Contact(
                    realmObject._id,
                    realmObject.name,
                    realmObject.surname,
                    realmObject.number,
                    realmObject.email,
                )
            )
        }
        return contacts
    }

    override fun insert(contact: Contact) {
        realm.writeBlocking {
            copyToRealm(
                ContactRealm(
                    _id = contact.id,
                    name = contact.name,
                    surname = contact.surname,
                    email = contact.email,
                    number = contact.number,
                )
            )
        }
    }

    override fun get(id: String): Contact {
        val realmObject = realm.query<ContactRealm>("_id == $0", id).find().first()
        return Contact(realmObject._id, realmObject.name, realmObject.surname, realmObject.number, realmObject.email)
    }

    override fun delete(id: String) {
        realm.writeBlocking {
            val frozenContact = realm.query<ContactRealm>("_id == $0", id).find().first()
            delete(findLatest(frozenContact)!!)
        }
    }

    override fun edit(contact: Contact) {
        realm.writeBlocking {
            val frozenContact = realm.query<ContactRealm>("_id == $0", contact.id).find().first()
            val defrostedContact = findLatest(frozenContact)!!
            defrostedContact.name = contact.name
            defrostedContact.surname = contact.surname
            defrostedContact.number = contact.number
            defrostedContact.email = contact.email
        }
    }
}