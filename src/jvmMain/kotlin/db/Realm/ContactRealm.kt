package db.Realm

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

open class ContactRealm(
    @PrimaryKey
    var _id: String = "",
    var name: String = "",
    var surname: String = "",
    var number: String = "",
    var email: String = "",
) : RealmObject {
    constructor() : this(name = "") {}

    override fun toString() = "ContactRealm($_id, $name, $surname, $number, $email)"
}