package model

class Contact(
    val id: String,
    var name: String,
    var surname: String,
    var number: String,
    var email: String,
) {
    override fun toString() = "Contact($name, $surname, $number, $email)"
}